import 'package:flutter/material.dart';


enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContactProfilePage());

}

class MyAppTheme{
  static ThemeData appThemeLight(){
    return
      ThemeData(
          brightness: Brightness.light,
          appBarTheme: AppBarTheme(
            color: Colors.blue,
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.indigo.shade500,
          )
      );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.blue,
            iconTheme: IconThemeData(
              color: Colors.white,

            )

        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }
}



var body = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          height: 50,

          //Height constraint at Container widget level

        ),
        Container(

          child: Center(
            child: Text(
              "อ.เมืองชลบุรี",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 30 ),
            ),
          ),

        ),
        Container(

          child: Center(
            child: Text(
              "22°",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 80 ),
            ),
          ),

        ),

        const Card(
          child: Padding(

            padding: EdgeInsets.all(16.0),
            child: Text('มีเมฆเป้นบางส่วนระหว่างเวลา 03:00-09:00 และคาดว่ามีแดดออกเป็นส่วนมากเวลา 9:00'),

          ),
          color: Colors.transparent,

        ),


        Card(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                 profileActionItem()
              ],
            ),
          ),
          color: Colors.transparent,
        ),

        Container(
          height: 60,

          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(

                  "พยากรอากาศ 5 วัน",
                  // style: TextStyle(fontSize: 30),
                ),

              ),
            ],
          ),
        ),
        Divider(
          color: Colors.black87,
        ),

        todayWeather(),
        Divider(
          color: Colors.black87,
        ),

        twodayWeather(),
        Divider(
          color: Colors.black87,
        ),

        threedayWeather(),

        Divider(
          color: Colors.black87,
        ),

        fourdayWeather(),
        Divider(
          color: Colors.black87,
        ),
        fivedayWeather(),
        Divider(
          color: Colors.black87,
        ),


      ],
    ),
  ],
);
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          :MyAppTheme.appThemeDark(),

      home: Container(
        constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("./img/bg.jpg"), fit: BoxFit.cover),
          ),

        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: body,

        ),
      ),



    );
  }
}


Widget nowWeather() {
  return Column(
    children: <Widget>[
      Text("ตอนนี้"),
      IconButton(

        icon: Icon(
          Icons.nightlight,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("22°"),
    ],
  );
}

Widget twoWeather() {
  return Column(
    children: <Widget>[
      Text("02"),
      IconButton(
        icon: Icon(
          Icons.nightlight,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("22°"),
    ],
  );
}

Widget threeWeather() {
  return Column(
    children: <Widget>[
      Text("03"),
      IconButton(
        icon: Icon(
          Icons.nightlight,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("22°"),
    ],
  );
}

Widget fourWeather() {
  return Column(
    children: <Widget>[
      Text("04"),
      IconButton(
        icon: Icon(
          Icons.nightlight,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("22°"),
    ],
  );
}

Widget fiveWeather() {
  return Column(
    children: <Widget>[
      Text("05"),
      IconButton(
        icon: Icon(
          Icons.nightlight,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("22°"),
    ],
  );
}

Widget sixWeather() {
  return Column(
    children: <Widget>[
      Text("06"),
      IconButton(
        icon: Icon(
          Icons.sunny_snowing,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("22°"),
    ],
  );
}
Widget sevenWeather() {
  return Column(
    children: <Widget>[
      Text("07"),
      IconButton(
        icon: Icon(
          Icons.sunny_snowing,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("23°"),
    ],
  );
}
Widget eightWeather() {
  return Column(
    children: <Widget>[
      Text("08"),
      IconButton(
        icon: Icon(
          Icons.sunny,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("24°"),
    ],
  );
}
Widget nineWeather() {
  return Column(
    children: <Widget>[
      Text("09"),
      IconButton(
        icon: Icon(
          Icons.sunny,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("25°"),
    ],
  );
}
Widget tenWeather() {
  return Column(
    children: <Widget>[
      Text("10"),
      IconButton(
        icon: Icon(
          Icons.sunny,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("27°"),
    ],
  );
}
Widget elevenWeather() {
  return Column(
    children: <Widget>[
      Text("11"),
      IconButton(
        icon: Icon(
          Icons.sunny,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("30°"),
    ],
  );
}
Widget noonWeather() {
  return Column(
    children: <Widget>[
      Text("ตอนเที่ยง"),
      IconButton(
        icon: Icon(
          Icons.sunny,
          // color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("31°"),
    ],
  );
}


//Listtile
Widget todayWeather(){
  return ListTile(
    leading:Text("วันนี้        ") ,
    title: Icon(Icons.sunny),

    trailing:Text("22° -> 31°"
    ) ,
    onTap: () {

    },
  );

}

Widget twodayWeather(){
  return ListTile(
    leading:Text("อ.           ") ,
    title: Icon(Icons.cloud),

    trailing:Text("23° -> 31°"
    ) ,
    onTap: () {

    },
  );

}
Widget threedayWeather(){
  return ListTile(
    leading:Text("พ.           ") ,
    title: Icon(Icons.cloud),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}

Widget fourdayWeather(){
  return ListTile(
    leading:Text("พฤ.          ") ,
    title: Icon(Icons.cloud),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}
Widget fivedayWeather(){
  return ListTile(
    leading:Text("ศ.           ") ,
    title: Icon(Icons.cloud),

    trailing:Text("22° -> 32°"
    ) ,
    onTap: () {

    },
  );

}





Widget profileActionItem(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      nowWeather(),
      twoWeather(),
      threeWeather(),
      fourWeather(),
      fiveWeather(),
      sixWeather(),
      sevenWeather(),
      eightWeather(),
      nineWeather(),
      tenWeather(),
      elevenWeather(),
      noonWeather(),


    ],
  );
}
